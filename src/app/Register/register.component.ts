import { Component } from '@angular/core';
import axios from 'axios';
import { Router } from '@angular/router';
import { loginStatus } from '../loginStatus';


const http = axios.create({
  baseURL: 'http://localhost:3000/api'
});

@Component({
  selector: 'app-root',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {

  constructor(private router: Router) {

  }


  username = '';
  password = '';
  passwordRepeat = '';

  usernameVal(value: string) {
    this.username = value;


  };
  passwordVal(value: string) {
    this.password = value;


  }
  passwordValRepeat(value: string) {
    this.passwordRepeat = value;


  }

  novoCadastro() {

    if (this.password !== this.passwordRepeat) {

      alert('Senhas divergentes')

      return

    }

    http.get('http://localhost:3000/usuario/getall').then((req) => {

      let usersList = req.data;

      let userNameExists = false;

      for (let idx = 0; idx < usersList.length; idx++) {


        if (usersList[idx].nome === this.username) {

          userNameExists = true;

          alert('Nome de usuario ja em uso')

          usersList = false;

          return

        }

      }

      http.post('http://localhost:3000/usuario/cadastrar', { 'nome': this.username, 'senha': this.password }).then((res) => {


        console.log('cadastro feito')

        loginStatus.autorizado = true;
        loginStatus.usuario = this.username;
        usersList = false;

        http.get('http://localhost:3000/usuario/getall').then((req) => {

          usersList = req.data;


          for (let idx = 0; idx < usersList.length; idx++) {


            if (usersList[idx].nome === this.username) {

              loginStatus.userId = usersList[idx].id;

              this.username = '';
              this.password = '';
              this.passwordRepeat = '';
              usersList = false;

              return this.router.navigateByUrl('/home')

            }


          }

          return
        })


      })


    })








  }

}
