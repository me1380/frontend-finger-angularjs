import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./Home/home.component";
import { LoginComponent } from "./Login/login.component";
import { RegisterComponent } from "./Register/register.component";


const APP_ROUTES:Routes = [
    {path:'home',component:HomeComponent},
    {path:'',component:LoginComponent},
    {path:'register',component:RegisterComponent}

];

export const routing:ModuleWithProviders<any> = RouterModule.forRoot(APP_ROUTES);


