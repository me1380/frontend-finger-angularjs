import { Component } from '@angular/core';
import axios from 'axios';
import { Router } from '@angular/router';
import { loginStatus } from '../loginStatus';

const http = axios.create({
  baseURL: 'http://localhost:3000'
});

@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent {

  constructor(private router: Router) {

    if (!loginStatus.autorizado) {
      this.router.navigateByUrl('')
      return
    }

  }
  nome ='Ola ' +  loginStatus.usuario;
  oldPassword = '';
  newPassword = '';

  oldPasswordVal(value: string) {
    this.oldPassword = value;
  };
  newPasswordVal(value: string) {
    this.newPassword = value;
  }

  trocarSenha() {

    if (!this.oldPassword || !this.newPassword) {

      alert('campos vazios')

      return

    }

    http.get('http://localhost:3000/usuario/getall').then((req) => {

      let usersList = req.data;

      for (let idx = 0; idx < usersList.length; idx++) {

        if (usersList[idx].nome === loginStatus.usuario) {

          if (usersList[idx].senha === this.oldPassword) {

            http.put('/usuario/edit/' + loginStatus.userId, { senha: this.newPassword }).then((res) => {

              usersList = false;
              this.oldPassword = '';
              this.newPassword = '';

              alert('senha alterada com sucesso')

            }).catch((err) => {

              console.log(err)

              usersList = false;
              this.oldPassword = '';
              this.newPassword = '';

            })

            return

          }

        }

      }

      usersList = false;

      alert('senha antiga incorreta')

      return

    }).catch((err) => {

      console.log(err)

      this.oldPassword = '';
      this.newPassword = '';


    })


  }

  logOut() {

    this.oldPassword = '';
    this.newPassword = '';
    loginStatus.autorizado = false;
    loginStatus.userId = '';
    loginStatus.usuario = '';

    this.router.navigateByUrl('')

    return

  }

}
