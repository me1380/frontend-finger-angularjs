import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LoginComponent } from './login.component';


@NgModule({
  declarations: [
    LoginComponent,
  
  ],
  imports: [
    BrowserModule,
    Router
 
  ],
  providers: [],
  bootstrap: [LoginComponent]
})
export class LoginModule { }
