import { Component } from '@angular/core';
import axios from 'axios';
import { loginStatus } from '../loginStatus';
import { NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';

const http = axios.create({
  baseURL: 'http://localhost:3000/api'
});


@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private router: Router) {
  }

  username = '';
  password = '';
  usernameVal(value: string) {
    this.username = value;
  };
  passwordVal(value: string) {
    this.password = value;
  }
  login() {

    if (!this.password || !this.username) {

      alert('campos vazios')
      return

    }

    http.get('http://localhost:3000/usuario/getall').then((req) => {

      let usersList = req.data;

      for (let idx = 0; idx < usersList.length; idx++) {


        if (usersList[idx].nome.toLowerCase() === this.username.toLowerCase()) {

          if (usersList[idx].senha === this.password) {

            loginStatus.autorizado = true;

            loginStatus.usuario = usersList[idx].nome;
            loginStatus.userId = usersList[idx].id;

            this.username = '';
            this.password = '';
            usersList = false;

            this.router.navigateByUrl('/home')

            return

          }

          this.password = '';

          alert('senha incorreta')

          usersList = false;
          return

        }


      }

      usersList = false;

      alert('Conta não existe')

      return

    })


  }

  loadRegisterPage() {

    this.router.navigateByUrl('/register')

    return
  }



}
