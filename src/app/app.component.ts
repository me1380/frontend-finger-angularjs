import { Component } from '@angular/core';
import axios from 'axios';

const http = axios.create({
baseURL:'http://localhost:3000/api'
});

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'projeto';
  username = '';
  password = '';
  usernameVal(value: string){
    this.username = value;
  };
  passwordVal(value: string){
  this.password = value;
  }
  login(){

    if(!this.password || !this.username){

      console.log('campos vazios')
      return

    }

    console.log(this.password,this.username)

    http.get('http://localhost:3000/usuario/getall').then((req)=>{

    const usersList = req.data;
    
    console.log(usersList)

    let userExist = false;
    let passwordMatches = false;

    for(let idx = 0; idx < usersList.length ; idx++){

      if(usersList[idx].nome === this.username){

        userExist = true;

        if(usersList[idx].senha === this.password){

          passwordMatches = true;

          console.log('Login e senhas corretos')

          this.username = '';
          this.password = '';

          return {login:true,usuario:this.username}

        }

          this.password = '';

        console.log('senha incorreta')

        return {login:false,error:'Senha incorreta'}

      }


    }

    this.username = '';
          this.password = '';

    console.log('Conta não existe')

    return {login:false,error:'Conta não existe'}

    })


  }

  novoCadastro(){

    http.post('http://localhost:3000/usuario/cadastrar',{'nome':'Jesus','senha':'cristh123'}).then((res)=>{
      console.log('feito')
    })

  }

}
